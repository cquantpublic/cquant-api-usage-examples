#!/usr/bin/python3
import json
import requests
import time
import urllib.request
import argparse
import os

API_SERVER = "https://DOMAINNAME/api/v2/"
API_KEY    = "REPLACE_ME"

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument( 'jsonPath',
                         metavar='path',
                         type=str,
                         help='Path to json run configuration file.')
    return parser.parse_args()

def mylistdir(directory):
    """A specialized version of os.listdir() that ignores files that
    start with a leading period."""
    filelist = os.listdir(directory)
    return [x for x in filelist
            if not (x.startswith('.'))]

## CREATE THE MODEL RUN 
def createModel(rundetails):
    response = requests.post(API_SERVER+"models/"+rundetails['compositeModelTag']+"/modelRun",
        headers={"api_key":API_KEY})
    response.raise_for_status()
    json_data = response.json()
    print(json_data)
    rundetails['compositeModelRunId'] = json_data['compositeRunId']
    print("Created Composite Run id", str(json_data['compositeRunId']))

## Rename, Save, Delete run.
def modifyRun(rundetails):
    if 'runConfig' in rundetails:
        # 'title', 'retain', and 'delete' are what API uses.
        print('Titled:', rundetails["runConfig"]["runTitle"])
        modifiers = {} 
        if ("runTitle" in rundetails["runConfig"]):
            modifiers = {'title': rundetails["runConfig"]["runTitle"]}
            response = requests.put(API_SERVER+"modelRuns/"+str(rundetails['compositeModelRunId']),
                    headers={"api_key":API_KEY}, json =  modifiers)
            response.raise_for_status()
        if ("saveRun" in rundetails["runConfig"]):
            modifiers = {'retain': rundetails["runConfig"]["saveRun"]}
            response = requests.put(API_SERVER+"modelRuns/"+str(rundetails['compositeModelRunId'])+"/retain",
                    headers={"api_key":API_KEY}, json =  modifiers)
            response.raise_for_status()


## UPDATE THE MODEL RUN PARAMS; 
## If no model inputs are specified, this step is skipped
## Only values provided in json are updated
## Defaults are assumed for the rest 
def updateParams(rundetails):
    print("Updating parameters")
    data = {}
    if 'parameters' in rundetails:
        for p in rundetails['parameters']:
            data[p['name']] = p['value']
        response = requests.put(API_SERVER+"modelRuns/"+str(rundetails['compositeModelRunId'])+"/runParams",
            headers={"api_key":API_KEY}, json = data)
        print(response.json())
        response.raise_for_status()

def putInput(rundetails, f):
    response = requests.put(API_SERVER+"modelRuns/"+str(rundetails['compositeModelRunId'])+"/runInputs",
                            headers={"api_key":API_KEY}, files=f)
    response.raise_for_status()

## UPLOAD THE INPUT FILES FOR THE MODEL
## If no model inputs are specified, this step is skipped
def updateInputs(rundetails): 
    print("Updating model inputs")
    if 'inputs' in rundetails:
        ## BEFORE UPLOADING NEW FILES, DELETE ALL FILES FROM PREVIOUS RUN 
        filesToDelete = {}
        filesToDelete["fileTypes"] = []
        for p in rundetails['inputs']:
            filesToDelete["fileTypes"].append(p["name"])

        response = requests.delete(API_SERVER+"modelRuns/"+str(rundetails['compositeModelRunId'])+"/runInputs",
                    headers={"api_key":API_KEY}, json=filesToDelete)
        response.raise_for_status()

        if 'dataSourceConfig' in rundetails: # If a data source is specified
            print("uploading data source config file..", rundetails['dataSourceConfig'])
            dsFile = {'dataSourceConfig': open(rundetails['dataSourceConfig'], 'rb')}
            response = requests.put(API_SERVER+"modelRuns/"+str(rundetails['compositeModelRunId'])+"/uploadConfigFile", 
                        headers={"api_key":API_KEY},files=dsFile)
            response.raise_for_status()
        for p in rundetails['inputs']:
            if isinstance(p["value"],str): # If single file or directory
                if os.path.isdir(p["value"]): # If directory
                    print("uploading directory..",p["value"])
                    for file in mylistdir(p["value"]):
                        f = {}
                        f[p['name']] = open( os.path.join(p['value'], file), 'rb' )
                        putInput(rundetails, f)
                else : # If single file
                    print("uploading..",p["value"])
                    f = {}
                    f[p['name']] = open(p["value"], 'rb')
                    putInput(rundetails, f)
            else: # Multiple files for the given input
                for elem in p["value"]:
                    print("uploading..",elem)
                    f = {}
                    f[p['name']] = open(elem, 'rb')
                    putInput(rundetails, f)
                
## START THE MODEL RUN
def runModel(rundetails):
    print("Starting the model run")
    data = {}
    data["status"] = "start"
    response = requests.put(API_SERVER+"modelRuns/"+str(rundetails['compositeModelRunId'])+"/status",
    headers={"api_key":API_KEY}, json =  data)
    response.raise_for_status()
    print(response.text)

## POLL FOR MODEL RUN COMPLETE 
## In production environments, this should be done using webhooks
## This implementation checks if a model is running or not every 5 seconds
def waitForComplete(rundetails):
    print("Waiting for model run to complete...")
    executionState = "Running"
    while executionState != "Complete" and  executionState != "Failed":
        time.sleep( 30 )
        response = requests.get(API_SERVER+"modelRuns/"+str(rundetails['compositeModelRunId'])+"/status",
        headers={"api_key":API_KEY})
        response.raise_for_status()
        json_data = response.json()
        print(json_data)
        executionState = json_data["executionState"]
    rundetails["executionState"] = executionState

## Download Result Files
def downloadResults(rundetails):
    response = requests.get(API_SERVER+"modelRuns/"+str(rundetails['compositeModelRunId'])+"/runOutputs",
    headers={"api_key":API_KEY})
    response.raise_for_status()
    json_data = response.json()

    opener = urllib.request.build_opener()
    opener.addheaders = [('api_key', API_KEY)]
    urllib.request.install_opener(opener)

    outputDir = rundetails["runConfig"]["outputDir"]
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)
    
    otherOutputs = list()

    print( "Downloading model results to", os.path.join(os.getcwd(), outputDir) )
    for elem in json_data["results"]:
        # If the array is missing or empty, download all files, otherwise download specified files only.
        if 'outputsToDownload' not in rundetails or len(rundetails['outputsToDownload']) == 0 or elem['outputName'] in rundetails['outputsToDownload']:
            print("Downloading output name:", elem["outputName"] )
            urllib.request.urlretrieve( elem["fileUrl"], os.path.join( outputDir, elem["fileName"] ) )
        else:
            otherOutputs.append(elem["outputName"])
    
    if len(otherOutputs) > 0:
        print('Other outputs available for download:')
        print(otherOutputs)



## Main function
if __name__ == '__main__':
    args = parse_args()

    with open(args.jsonPath) as json_data_file:
        rundetails = json.load(json_data_file)

    if('compositeModelRunId' in rundetails):
        print("Composite model id found - updating an existing model run")
    else:
        print("Composite model id not found - creating a new model run")
        createModel(rundetails)

    modifyRun(rundetails)

    updateParams(rundetails)
    
    updateInputs(rundetails)

    runModel(rundetails)

    waitForComplete(rundetails)

    if(rundetails["executionState"] == "Complete" and 'runConfig' in rundetails):
        if(rundetails["runConfig"]["downloadResults"]):
            downloadResults(rundetails)

    if(rundetails["executionState"] == "Complete"):
        print('Model Run Done.')
