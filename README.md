# cQuant API Usage Examples

Using the cQuant API requires an API key. Please reach out to cQuant support to request an API key. 

## Notes
To update an existing model run, add the compositeRunId to the model json (e.g. "compositeRunId":448421).
Retreive a compositeRunId for an existing model run with GET /v1/modelRuns/search.


## NodeJS instructions:
- run `npm install`
- modify the model `json` as needed
- run `node cquantAPIExample.js`
